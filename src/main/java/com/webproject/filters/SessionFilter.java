package com.webproject.filters;

import com.webproject.service.SessionDecrypt;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;

public class SessionFilter extends MainFilter {
    private static final String COOKIE_NAME = "PERSON";
    private static SessionDecrypt sessionDecrypt = new SessionDecrypt();
    private static ThreadLocal<HttpServletRequest> requestLocalStorage = new ThreadLocal<>();
    private static ThreadLocal<HttpServletResponse> responseLocalStorage = new ThreadLocal<>();

    @Override
    public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException, ServletException {

    }

    private void loadAttributes(HttpServletRequest req)  {
        Map<String, Object> attributesFromCookie = getSessionAttributesFromCookie(req);
        if (attributesFromCookie != null) {
            HttpSession session = req.getSession(true);
            for (Map.Entry<String, Object> entry : attributesFromCookie.entrySet()) {
                session.setAttribute(entry.getKey(), entry.getValue());
            }
        }
    }

    private Map<String, Object> getSessionAttributesFromCookie(HttpServletRequest req) {
        Cookie cookie = null;
        for (Cookie c : req.getCookies()) {
            if (c.getName().equals(COOKIE_NAME)) cookie = c;
        }

        Map<String, Object> attributes = null;
        if (cookie != null) {
            attributes = sessionDecrypt.decrypt(cookie.getValue());
        }
        return attributes;
    }

    public static void update() {
        if (requestLocalStorage.get() != null && responseLocalStorage.get() == null) {return;}
        HttpSession session = requestLocalStorage.get().getSession(false);
        if (session != null && session.getAttributeNames().hasMoreElements()) {
            Cookie cookie = new Cookie(COOKIE_NAME, sessionDecrypt.encrypt(session));
            cookie.setPath("/");
            responseLocalStorage.get().addCookie(cookie);
        }
    }
//
//    public String info(HttpSession session, Model model) {
//        Integer counter = (Integer) session.getAttribute("counter");
//        if (counter == null) {
//            counter = 0;
//        }
//        counter++;
//        session.setAttribute("counter", counter);
//        model.addAttribute("counter", counter);
//        update();
//        return "index";
//    }
}
