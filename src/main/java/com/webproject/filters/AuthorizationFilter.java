package com.webproject.filters;

import com.webproject.entity.User;
import org.apache.log4j.Logger;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.Serializable;
import java.util.Enumeration;

public class AuthorizationFilter extends MainFilter {
    final static Logger logger = Logger.getLogger(AuthorizationFilter.class);

    @Override
    public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {

        final String path = request.getRequestURI().substring(request.getContextPath().length());

        HttpSession session = request.getSession();

        synchronized (session) {

            if (logger.isDebugEnabled()) {
                Enumeration attributeNames = session.getAttributeNames();
                logger.debug("Attribute names exists? " + attributeNames.hasMoreElements() +
                        ", Person exists? " + session.getAttribute("PERSON"));
                while (attributeNames.hasMoreElements()) {
                    String key = (String) attributeNames.nextElement();
                    String value = session.getAttribute(key).toString();
                    logger.info("Session attribute name = \"" + key +
                            "\", key = " + value + " , path is \"" + path + "\"");
                }
            }

            User user = (User) session.getAttribute("PERSON"); //TODO User не сериализуется

            //если сессии нет
            if (session.getAttribute("PERSON") == null) {
                //переход осуществляется на /home или /logout, тогда пропускаем
                if ("/home".equals(path)) {
                    filterChain.doFilter(request, response);
                    return;
                }
                //если сессии нет, и попытка перейти на закрытые страницы, перенаправляем логиниться
                response.sendRedirect("/home");
                return;
            }
            if (Serializable.class.isInstance(user)) {
                logger.debug("Login from PERSON : " + user.getLogin());
            }
            //атрибут сессии существует, и пытается перейти на /home, перенаправили на личную страницу
            if ("/home".equals(path)) {
                response.sendRedirect("/user");
                return;
            }

            //сессия существует, и переходит на личную страницу, пропускаем
            if ("/user".equals(path)) {
                filterChain.doFilter(request, response);
            }
        }
    }

    @Override
    public void destroy() {
        super.destroy();
        logger.info(">>> Destroy authorization filter.");
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        super.init(filterConfig);
        logger.info(">>> Init authorization filter.");

    }
}

