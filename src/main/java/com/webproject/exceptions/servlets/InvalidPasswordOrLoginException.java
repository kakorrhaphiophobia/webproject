package com.webproject.exceptions.servlets;

public class InvalidPasswordOrLoginException extends RuntimeException {
    public InvalidPasswordOrLoginException() {
        super();
    }

    public InvalidPasswordOrLoginException(String message) {
        super(message);
    }

    public InvalidPasswordOrLoginException(String message, Throwable cause) {
        super(message, cause);
    }
}
