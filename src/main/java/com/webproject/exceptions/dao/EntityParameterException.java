package com.webproject.exceptions.dao;

public class EntityParameterException extends DaoException {
    public EntityParameterException() {
        super();
    }

    public EntityParameterException(String message) {
        super(message);
    }

    public EntityParameterException(String message, Throwable cause) {
        super(message, cause);
    }
}
