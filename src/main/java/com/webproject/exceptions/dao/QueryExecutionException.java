package com.webproject.exceptions.dao;
/**
 * При невозможносит извлеч данные из хранилища.
 * */
public class QueryExecutionException extends DaoException {
    public QueryExecutionException() {
        super();
    }

    public QueryExecutionException(String message) {
        super(message);
    }

    public QueryExecutionException(String message, Throwable cause) {
        super(message, cause);
    }
}
