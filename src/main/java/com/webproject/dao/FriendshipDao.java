package com.webproject.dao;

import com.webproject.entity.Friendship;
import com.webproject.exceptions.dao.DaoException;
import com.webproject.exceptions.dao.EntityNotFoundException;
import com.webproject.exceptions.dao.EntityParameterException;
import com.webproject.exceptions.dao.QueryExecutionException;

import javax.sql.DataSource;
import javax.sql.PooledConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FriendshipDao implements GenericDao<Friendship, String> {

    private final DataSource dataSource;

    public FriendshipDao(final DataSource dataSource) {
        if (dataSource == null) {
            throw new IllegalArgumentException("Datasource is null, may be connection is not established or broken.");
        }
        this.dataSource = dataSource;

    }

    /**
     * Формирует объект Дружбы пользователей согласно переданному запросу.
     */
    private Friendship generateFriendship(ResultSet rs) throws DaoException, SQLException {
        final Friendship friendship = new Friendship();
        while (rs.next()) {
            friendship.setUserOne(rs.getString("user_one"));
            friendship.setUserTwo(rs.getString("user_two"));
            friendship.setStatus(rs.getInt("status"));
            friendship.setDateRequest(rs.getDate("date_friends"));
            return friendship;
        }
        return friendship;
    }

    /**
     * Создание записи о дружбе двух Пользователей.
     */
    @Override
    public void create(Friendship friendship) throws DaoException {
        final String sql = "INSERT INTO Friendship" +
                "(user_one, user_two, status, date_friends) VALUES " +
                "(?,?,?,?)";
        try (Connection conn = dataSource.getConnection();
             Connection actual = ((PooledConnection) conn).getConnection();
             PreparedStatement statement = actual.prepareStatement(sql)
        ) {
            statement.setString(1, friendship.getUserOne());
            statement.setString(2, friendship.getUserTwo());
            statement.setInt(3, friendship.getStatus());
            statement.setDate(4, friendship.getDateRequest());
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            throw new QueryExecutionException(e.getMessage(), e);
        }
    }

    @Override
    public Friendship findByLogin(String login) throws DaoException {
        throw new UnsupportedOperationException("If you want find all user friends, " +
                "use friendship.findAllFriendsByLogin method.");
    }

    /**
     * Возвращает список из логинов всех друзей (User) по логину Пользователя.
     */
    public List<String> findAllByLogin(String login) throws DaoException {
        final String sql = "SELECT * FROM Friendship WHERE status = 0 AND (user_one = ? OR user_two = ?)";
        final List<String> friendsList = new ArrayList<>();
        if (login == null) {
            throw new EntityParameterException("Login = null.");
        }
        try (Connection conn = dataSource.getConnection();
             Connection actual = ((PooledConnection) conn).getConnection();
             PreparedStatement preparedStatement = actual.prepareStatement(sql)
        ) {
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, login);
            try (ResultSet resultSet = preparedStatement.executeQuery()
            ) {
                if (!resultSet.isBeforeFirst()) {
                    throw new EntityNotFoundException("Friends not found.");
                } else {
                    while (resultSet.next()) {
                        if (login.equals(resultSet.getString("user_one"))) {
                            friendsList.add(resultSet.getString("user_two"));
                        } else {
                            friendsList.add(resultSet.getString("user_one"));
                        }
                    }
                }
            }
        } catch (SQLException e) {
            throw new QueryExecutionException(e.getMessage(), e);
        }
        return friendsList;
    }

    /**
     * Обновляет статус дружбы согласно переданному объекту.
     */
    @Override
    public void update(Friendship friendship) throws DaoException {
        final String sql = "UPDATE Friendship SET status = ? WHERE user_one = ? AND user_two = ?";
        try (Connection conn = dataSource.getConnection();
             Connection actual = ((PooledConnection) conn).getConnection();
             PreparedStatement preparedStatement = actual.prepareStatement(sql)
        ) {
            preparedStatement.setInt(1, friendship.getStatus());
            preparedStatement.setString(2, friendship.getUserOne());
            preparedStatement.setString(3, friendship.getUserTwo());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new QueryExecutionException(e.getMessage(), e);
        }
    }

    @Override
    public void delete(Friendship object) throws DaoException {
        throw new UnsupportedOperationException("If you want to delete friendship, use user.delete method.");
    }

    /**
     * Возвращает список всех существующих записей о дружбе в БД.
     */
    @Override
    public List<Friendship> findAll() throws DaoException {
        final List<Friendship> friendshipList = new ArrayList<>();
        final String sql = "SELECT * FROM Friendship";
        try (Connection conn = dataSource.getConnection();
             Connection actual = ((PooledConnection) conn).getConnection();
             PreparedStatement preparedStatement = actual.prepareStatement(sql);
             ResultSet resultSet = preparedStatement.executeQuery()
        ) {
            while (resultSet.next()) {
                Friendship friendship = generateFriendship(resultSet);
                friendshipList.add(friendship);
            }
        } catch (SQLException e) {
            throw new QueryExecutionException(e.getMessage(), e);
        }
        return friendshipList;
    }
}
