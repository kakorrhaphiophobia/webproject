package com.webproject.dao;

import com.webproject.exceptions.dao.DaoException;

import java.io.Serializable;
import java.util.List;

/**
 * Унифицированный интерфейс управления персистентным состоянием объектов
 *
 * @param <T>  тип объекта базы данных
 * @param <PK> тип первичного ключа
 */
public interface GenericDao<T extends Serializable, PK extends Serializable> {

    /**
     * Создает новую запись согласно соответствующему объекту.
     */
    public void create(T objects) throws DaoException;

    /**
     * Возвращает объект соответствующий записи с первичным ключом key.
     */
    public T findByLogin(PK key) throws DaoException;

    /**
     * Сохраняет обновленное состояние объекта в базе данных.
     */
    public void update(T object) throws DaoException;

    /**
     * Удаляет объект в базе данных.
     */
    public void delete(T object) throws DaoException;

    /**
     * Возвращает список объектов соответствующих всем записям в базе данных.
     */
    public List<T> findAll() throws DaoException;
}
