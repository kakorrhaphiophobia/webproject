package com.webproject.dao;

import com.webproject.entity.Chat;
import com.webproject.exceptions.dao.DaoException;
import com.webproject.exceptions.dao.EntityNotFoundException;
import com.webproject.exceptions.dao.EntityParameterException;
import com.webproject.exceptions.dao.QueryExecutionException;

import javax.sql.DataSource;
import javax.sql.PooledConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ChatDao implements GenericDao<Chat, String> {

    private final DataSource dataSource;

    public ChatDao(final DataSource dataSource) {
        if (dataSource == null) {
            throw new IllegalArgumentException("Datasource is null, may be connection is not established or broken.");
        }
        this.dataSource = dataSource;
    }

    /**
     * Формирует объект Диалога пользователей согласно переданному запросу.
     */
    private Chat generateChat(ResultSet rs) throws DaoException, SQLException {
        final Chat chat = new Chat();
        if (!rs.isBeforeFirst()) {
            throw new EntityNotFoundException("Chat not found.");
        } else {
            while (rs.next()) {
                chat.setUserOne(rs.getString("user_one"));
                chat.setUserTwo(rs.getString("user_two"));
                chat.setDateCreation(rs.getTimestamp("date_creation"));
                return chat;
            }
        }
        return chat;
    }

    /**
     * Создает новую запись чата соглавно переданному объекту.
     */
    @Override
    public void create(Chat chat) throws DaoException {
        final String sql = "INSERT INTO Chat" +
                "(user_one, user_two, date_creation) VALUES " +
                "(?,?,?)";
        try (Connection conn = dataSource.getConnection();
             Connection actual = ((PooledConnection) conn).getConnection();
             PreparedStatement statement = actual.prepareStatement(sql)
        ) {
            statement.setString(1, chat.getUserOne());
            statement.setString(2, chat.getUserTwo());
            statement.setTimestamp(3, chat.getDateCreation());
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            throw new QueryExecutionException(e.getMessage(), e);
        }
    }

    /**
     * Находит диалог по переданным login.
     */
    public Chat findByLogin(String loginOne, String loginTwo) throws DaoException {
        final String sql = "SELECT * FROM Chat WHERE (user_one = ? AND user_two = ?) " +
                "OR (user_one = ? AND user_two = ?)";
        if (loginOne == null || loginTwo == null) {
            throw new EntityParameterException("Login = null.");
        }
        try (Connection conn = dataSource.getConnection();
             Connection actual = ((PooledConnection) conn).getConnection();
             PreparedStatement preparedStatement = actual.prepareStatement(sql)
        ) {
            preparedStatement.setString(1, loginOne);
            preparedStatement.setString(2, loginTwo);
            preparedStatement.setString(3, loginTwo);
            preparedStatement.setString(4, loginOne);
            try (ResultSet resultSet = preparedStatement.executeQuery()
            ) {
                final Chat chat = generateChat(resultSet);
                return chat;
            }
        } catch (SQLException e) {
            throw new QueryExecutionException(e.getMessage(), e);
        }
    }

    /**
     * По логину Пользователя возвращает список логинов всех Пользователей с которыми есть диалог.
     */
    public List<String> findAllByLogin(String login) throws DaoException {
        final String sql = "SELECT * FROM Chat WHERE user_one = ? OR user_two = ?";
        final List<String> chatList = new ArrayList<>();
        if (login == null) {
            throw new EntityParameterException("Login = null.");
        }
        try (Connection conn = dataSource.getConnection();
             Connection actual = ((PooledConnection) conn).getConnection();
             PreparedStatement preparedStatement = actual.prepareStatement(sql)
        ) {
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, login);
            try (ResultSet resultSet = preparedStatement.executeQuery()
            ) {
                while (resultSet.next()) {
                    if (login.equals(resultSet.getString("user_one"))) {
                        chatList.add(resultSet.getString("user_two"));
                    } else {
                        chatList.add(resultSet.getString("user_one"));
                    }
                }
                if (!chatList.isEmpty()) {
                    return chatList;
                } else throw new DaoException("Empty result.");
            }
        } catch (SQLException e) {
            throw new QueryExecutionException(e.getMessage(), e);
        }
    }

    /**
     * Не используется.
     */
    @Override
    public void update(Chat chat) throws DaoException {
        throw new UnsupportedOperationException("Not used.");
    }

    /**
     * Не используется.
     */
    @Override
    public void delete(Chat chat) throws DaoException {
        throw new UnsupportedOperationException("Not used.");
    }

    /**
     * Не используется.
     */
    @Override
    public Chat findByLogin(String key) throws DaoException {
        throw new UnsupportedOperationException("Not used.");
    }

    /**
     * Возвращает список всех диалогов.
     */
    @Override
    public List<Chat> findAll() throws DaoException {
        final List<Chat> chatList = new ArrayList<>();
        final String sql = "SELECT * FROM Friendship";
        try (Connection conn = dataSource.getConnection();
             Connection actual = ((PooledConnection) conn).getConnection();
             PreparedStatement preparedStatement = actual.prepareStatement(sql);
             ResultSet resultSet = preparedStatement.executeQuery()
        ) {
            while (resultSet.next()) {
                Chat chat = generateChat(resultSet);
                chatList.add(chat);
            }
        } catch (SQLException e) {
            throw new QueryExecutionException(e.getMessage(), e);
        }
        return chatList;
    }
}
