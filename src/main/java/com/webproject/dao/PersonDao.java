package com.webproject.dao;

import com.webproject.entity.Person;
import com.webproject.exceptions.dao.DaoException;
import com.webproject.exceptions.dao.EntityNotFoundException;
import com.webproject.exceptions.dao.EntityParameterException;
import com.webproject.exceptions.dao.QueryExecutionException;

import javax.sql.DataSource;
import javax.sql.PooledConnection;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PersonDao implements GenericDao<Person, String> {

    private final DataSource dataSource;

    public PersonDao(final DataSource dataSource) {
        if (dataSource == null) {
            throw new IllegalArgumentException("Datasource is null, may be connection is not established or broken.");
        }
        this.dataSource = dataSource;
    }

    /**
     * Создание записи о персонализации Пользователя согласно переданному объекту.
     */
    @Override
    public void create(Person person) throws DaoException {
        final String sql = "INSERT INTO PERSON" +
                "(login, firstname, lastname, gender, dob, country, address, telephone) VALUES " +
                "(?,?,?,?,?,?,?,?)";
        try (Connection conn = dataSource.getConnection();
             Connection actual = ((PooledConnection) conn).getConnection();
             PreparedStatement statement = actual.prepareStatement(sql)
        ) {
            statement.setString(1, person.getLogin());
            statement.setString(2, person.getFirstName());
            statement.setString(3, person.getLastName());
            statement.setString(4, person.getGender());
            statement.setDate(5, new Date(System.currentTimeMillis()));
            statement.setString(6, person.getCountry());
            statement.setString(7, person.getAddress());
            statement.setString(8, person.getTelephone());
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            throw new QueryExecutionException(e.getMessage(), e);
        }
    }

    /**
     * Проверяет результат, формирует объект Пользователь согласно переданному запросу.
     */
    private Person generatePerson(ResultSet rs) throws DaoException, SQLException {
        final Person person = new Person();
        if (!rs.isBeforeFirst()) {
            throw new EntityNotFoundException("Person not found.");
        } else {
            while (rs.next()) {
                person.setLogin(rs.getString("login"));
                person.setFirstName(rs.getString("firstname"));
                person.setLastName(rs.getString("lastname"));
                person.setGender(rs.getString("gender"));
                person.setDob(rs.getDate("dob"));
                person.setCountry(rs.getString("country"));
                person.setAddress(rs.getString("address"));
                person.setTelephone(rs.getString("telephone"));
                return person;
            }
        }
        return person;
    }

    /**
     * Формирует объект персонализации Пользователя согласно переданному ключу.
     */
    @Override
    public Person findByLogin(String login) throws DaoException {
        final String sql = "SELECT * FROM Person WHERE login = ?";
        if (login == null) {
            throw new EntityParameterException("Login = null.");
        }
        try (Connection conn = dataSource.getConnection();
             Connection actual = ((PooledConnection) conn).getConnection();
             PreparedStatement preparedStatement = actual.prepareStatement(sql)
        ) {
            preparedStatement.setString(1, login);
            try (ResultSet resultSet = preparedStatement.executeQuery()
            ) {
                return generatePerson(resultSet);
            }
        } catch (SQLException e) {
            throw new QueryExecutionException(e.getMessage(), e);
        }
    }

    /**
     * Перезаписывает изменные поля в таблице персонализации Пользователя в базе данных,
     * согласно переданному объекту.
     */
    @Override
    public void update(Person person) throws DaoException {
        final String sqlSelect = "SELECT * FROM Person WHERE login = ?";
        final String sqlUpdate = "UPDATE Person " +
                "SET firstname = ?, lastname = ?, gender = ?, dob = ?, country = ?, address = ?, telephone = ?" +
                " WHERE login = ?";
        /**
         * Пробуем получить объект Пользователя из БД, который нужно обновить.
         * */
        try (Connection conn = dataSource.getConnection();
             Connection actual = ((PooledConnection) conn).getConnection()
        ) {
            actual.setAutoCommit(false);
            try (PreparedStatement preparedStatement = actual.prepareStatement(sqlSelect)
            ) {
                preparedStatement.setString(1, person.getLogin());
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    Person personDB = generatePerson(resultSet);
                    /**
                     * И есть что обновлять.
                     * */
                    if (personDB.equals(person)) {
                        actual.rollback();
                        throw new QueryExecutionException
                                ("Object fields do not differ from the fields in the database.");
                    } else {
                        /**
                         * Передаем запрос на обновление.
                         * */
                        try (PreparedStatement preparedStatementUpdate = actual.prepareStatement(sqlUpdate)) {
                            preparedStatementUpdate.setString(1, person.getFirstName());
                            preparedStatementUpdate.setString(2, person.getLastName());
                            preparedStatementUpdate.setString(3, person.getGender());
                            preparedStatementUpdate.setDate(4, person.getDob());
                            preparedStatementUpdate.setString(5, person.getCountry());
                            preparedStatementUpdate.setString(6, person.getLogin());
                            preparedStatementUpdate.setString(7, person.getAddress());
                            preparedStatementUpdate.setString(8, person.getTelephone());
                            preparedStatementUpdate.executeUpdate();
                            actual.commit();
                        }
                    }
                }
            }
        } catch (SQLException e) {
            throw new QueryExecutionException(e.getMessage(), e);
        }
    }


    @Override
    public void delete(Person object) throws DaoException {
        throw new UnsupportedOperationException("If you want to delete person, use user.delete method.");
    }

    /**
     * Возвращает всех персонализированных Пользователей.
     */
    @Override
    public List<Person> findAll() throws DaoException {
        final List<Person> personList = new ArrayList<>();
        final String sql = "SELECT * FROM Person";
        try (Connection conn = dataSource.getConnection();
             Connection actual = ((PooledConnection) conn).getConnection();
             PreparedStatement preparedStatement = actual.prepareStatement(sql);
             ResultSet resultSet = preparedStatement.executeQuery()
        ) {
            while (resultSet.next()) {
                Person person = generatePerson(resultSet);
                personList.add(person);
            }
        } catch (SQLException e) {
            throw new QueryExecutionException(e.getMessage(), e);
        }
        return personList;
    }
}
