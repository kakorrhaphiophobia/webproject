package com.webproject.dao;

import com.webproject.entity.User;
import com.webproject.exceptions.dao.DaoException;
import com.webproject.exceptions.dao.EntityNotFoundException;
import com.webproject.exceptions.dao.EntityParameterException;
import com.webproject.exceptions.dao.QueryExecutionException;

import javax.sql.DataSource;
import javax.sql.PooledConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UsersDao implements GenericDao<User, String> {

    private DataSource dataSource;

    public UsersDao(DataSource dataSource) {
        if (dataSource == null) {
            throw new IllegalArgumentException("Datasource is null, may be connection is not established or broken.");
        }
        this.dataSource = dataSource;
    }

    /**
     * Создает новую запись в БД, соответствующую объекту User.
     */
    @Override
    public void create(User user) throws DaoException {
        final String sql = "INSERT INTO USERS" +
                "(login, pwd, email, date_registration, access_role, disabled) VALUES " +
                "(?,?,?,?,?,?)";
        try (Connection conn = dataSource.getConnection();
             Connection actual = ((PooledConnection) conn).getConnection();
             PreparedStatement statement = actual.prepareStatement(sql)
        ) {
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getEmail());
            statement.setTimestamp(4, user.getDateRegistration());
            statement.setString(5, user.getRole());
            /**
             * Новый пользователь, доступен по умолчанию.
             * */
            statement.setBoolean(6, false);
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            throw new QueryExecutionException(e.getMessage(), e);
        }
    }


    /**
     * Проверяет результат, формирует объект Пользователь согласно переданному запросу.
     */
    private User generateUser(ResultSet rs) throws DaoException, SQLException {
        final User user = new User();
        if (!rs.isBeforeFirst()) {
            throw new EntityNotFoundException("User not found.");
        } else {
            while (rs.next()) {//TODO Проверить кол-во строк в результате.
                user.setLogin(rs.getString("login"));
                user.setPassword(rs.getString("pwd"));
                user.setEmail(rs.getString("email"));
                user.setDateRegistration(rs.getTimestamp("date_registration"));
                user.setRole(rs.getString("access_role"));
                user.setDisable(rs.getBoolean("disabled"));
                return user;
            }
        }
        return user;//TODO поменять логику, чтобы не возвращать user
    }

    /**
     * Возвращает объект Пользователя соответсвующий записи в БД по login.
     */
    @Override
    public User findByLogin(String login) throws DaoException {
        final String sql = "SELECT * FROM Users WHERE login = ?";
        if (login == null) {
            throw new EntityParameterException("Login = null.");
        }
        try (Connection conn = dataSource.getConnection();
             Connection actual = ((PooledConnection) conn).getConnection();
             PreparedStatement preparedStatement = actual.prepareStatement(sql)
        ) {
            preparedStatement.setString(1, login);
            try (ResultSet resultSet = preparedStatement.executeQuery()
            ) {
                return generateUser(resultSet);
            }
        } catch (SQLException e) {
            throw new QueryExecutionException(e.getMessage(), e);
        }
    }

    /**
     * Перезаписывает изменные поля pwd и email Пользователя в базе данных, согласно переданному объекту.
     */
    @Override
    public void update(User user) throws DaoException {
        final String sqlSelect = "SELECT * FROM Users WHERE login = ?";
        final String sqlUpdate = "UPDATE Users SET pwd = ?, email = ? WHERE login = ?";
        /**
         * Пробуем получить объект Пользователя из БД, который нужно обновить.
         * */
        try (Connection conn = dataSource.getConnection();
             Connection actual = ((PooledConnection) conn).getConnection();
        ) {
            actual.setAutoCommit(false);
            try (PreparedStatement preparedStatement = actual.prepareStatement(sqlSelect)
            ) {
                preparedStatement.setString(1, user.getLogin());
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    User userDB = generateUser(resultSet);
                    /**
                     * И есть что обновлять.
                     * */
                    if (userDB.equals(user)) {
                        actual.rollback();
                        throw new QueryExecutionException
                                ("Object fields do not differ from the fields in the database.");
                    } else {
                        /**
                         * Передаем запрос на обновление.
                         * */
                        try (PreparedStatement preparedStatementUpdate = actual.prepareStatement(sqlUpdate)) {
                            preparedStatementUpdate.setString(1, user.getPassword());
                            preparedStatementUpdate.setString(2, user.getEmail());
                            preparedStatementUpdate.setString(3, user.getLogin());
                            preparedStatementUpdate.executeUpdate();
                            actual.commit();
                        }
                    }
                }
            }
        } catch (SQLException e) {
            throw new QueryExecutionException(e.getMessage(), e);
        }
    }

    /**
     * Удаляем пользователя из доступа.
     */
    @Override
    public void delete(User user) throws DaoException {
        final String sqlSelect = "SELECT disabled FROM Users WHERE login = ?";
        final String sqlUpdate = "UPDATE Users SET disabled = 'true' WHERE login = ?";

        /**
         * Получаем значение поля disabled.
         * */
        try (Connection conn = dataSource.getConnection();
             Connection actual = ((PooledConnection) conn).getConnection();
        ) {
            /**
             * Выключаем авто управление транзакциями.
             * */
            actual.setAutoCommit(false);
            try (PreparedStatement preparedStatement = actual.prepareStatement(sqlSelect)
            ) {
                preparedStatement.setString(1, user.getLogin());
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    Boolean fieldFromDbIsDisable = resultSet.getBoolean("disabled");
                    /**
                     * Если Disabled уже было установлено как true.
                     * */
                    if (fieldFromDbIsDisable) {
                        /**
                         * Отменяем изменения.
                         * */
                        actual.rollback();
                        throw new QueryExecutionException
                                ("Object fields do not differ from the fields in the database.");
                    } else {
                        /**
                         * Передаем запрос на обновление.
                         * */
                        try (PreparedStatement preparedStatementUpdate = actual.prepareStatement(sqlUpdate)) {
                            preparedStatementUpdate.setString(1, user.getLogin());
                            preparedStatementUpdate.executeUpdate();
                            /**
                             * Сохраняем изменения.
                             * */
                            actual.commit();
                        }
                    }
                }
            }
        } catch (SQLException e) {
            throw new QueryExecutionException(e.getMessage(), e);
        }
    }

    /**
     * Возвращает всех Пользователей.
     */
    @Override
    public List<User> findAll() throws DaoException {
        final List<User> usersList = new ArrayList<>();
        final String sql = "SELECT * FROM USERS";
        try (Connection conn = dataSource.getConnection();
             Connection actual = ((PooledConnection) conn).getConnection();
             PreparedStatement preparedStatement = actual.prepareStatement(sql);
             ResultSet resultSet = preparedStatement.executeQuery()
        ) {
            while (resultSet.next()) {
                User user = generateUser(resultSet);
                usersList.add(user);
            }
        } catch (SQLException e) {
            throw new QueryExecutionException(e.getMessage(), e);
        }
        return usersList;
    }
}
