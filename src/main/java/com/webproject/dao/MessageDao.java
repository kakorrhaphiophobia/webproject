package com.webproject.dao;

import com.webproject.entity.Message;
import com.webproject.exceptions.dao.DaoException;
import com.webproject.exceptions.dao.EntityNotFoundException;
import com.webproject.exceptions.dao.EntityParameterException;
import com.webproject.exceptions.dao.QueryExecutionException;

import javax.sql.DataSource;
import javax.sql.PooledConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MessageDao implements GenericDao<Message, String> {
    private final DataSource dataSource;

    public MessageDao(final DataSource dataSource) {
        if (dataSource == null) {
            throw new IllegalArgumentException("Datasource is null, may be connection is not established or broken.");
        }
        this.dataSource = dataSource;
    }

    /**
     * Создает запись в БД согласно переданному объекту.
     */
    @Override
    public void create(Message message) throws DaoException {
        final String sql = "INSERT INTO Message" +
                "(chat_id, sender, mtext, date_and_time) VALUES " +
                "(?,?,?,?)";
        try (Connection conn = dataSource.getConnection();
             Connection actual = ((PooledConnection) conn).getConnection();
             PreparedStatement statement = actual.prepareStatement(sql)
        ) {
            statement.setInt(1, message.getChatId());
            statement.setString(2, message.getSender());
            statement.setString(3, message.getMtext());
            statement.setTimestamp(4, message.getDateAndTime());
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            throw new QueryExecutionException(e.getMessage(), e);
        }
    }

    /**
     * Не используется
     */
    @Override
    public Message findByLogin(String login) throws DaoException {
        throw new UnsupportedOperationException("Not used.");
    }

    /**
     * Формирует объект Дружбы пользователей согласно переданному запросу.
     */
    private Message generateMessage(ResultSet rs) throws DaoException, SQLException {
        final Message message = new Message();
        while (rs.next()) {
            message.setChatId(rs.getInt("chat_id"));
            message.setSender(rs.getString("sender"));
            message.setMtext(rs.getString("mtext"));
            message.setDateAndTime(rs.getTimestamp("date_and_time"));
            return message;
        }
        return message;
    }

    /**
     * Найти все сообщения чата по chat_id
     */
    public List<Message> findByChatId(String chatId) throws DaoException {
        final String sql = "SELECT * FROM Message WHERE chat_id = ?";
        final List<Message> messageList = new ArrayList<>();
        if (chatId == null) {
            throw new EntityParameterException("Chat id = null.");
        }
        try (Connection conn = dataSource.getConnection();
             Connection actual = ((PooledConnection) conn).getConnection();
             PreparedStatement preparedStatement = actual.prepareStatement(sql)
        ) {
            preparedStatement.setString(1, chatId);
            try (ResultSet rs = preparedStatement.executeQuery()
            ) {
                if (!rs.isBeforeFirst()) {
                    throw new EntityNotFoundException("Message not found.");
                } else {
                    while (rs.next()) {
                        messageList.add(generateMessage(rs));
                    }
                }
            }
        } catch (SQLException e) {
            throw new QueryExecutionException(e.getMessage(), e);
        }
        return messageList;
    }

    /**
     * Найти все сообщения Пользователя в конкретном чате по chat_id и login
     */
    public List<Message> findByChatId(String chatId, String sender) throws DaoException {
        final String sql = "SELECT * FROM Message WHERE chat_id = ? AND sender = ?";
        final List<Message> messageList = new ArrayList<>();
        if (chatId == null || sender.equals(null)) {
            throw new EntityParameterException("Null argument.");
        }
        try (Connection conn = dataSource.getConnection();
             Connection actual = ((PooledConnection) conn).getConnection();
             PreparedStatement preparedStatement = actual.prepareStatement(sql)
        ) {
            preparedStatement.setString(1, chatId);
            preparedStatement.setString(2, sender);
            try (ResultSet rs = preparedStatement.executeQuery()
            ) {
                while (rs.next()) {
                    messageList.add(generateMessage(rs));
                }
            }
        } catch (SQLException e) {
            throw new QueryExecutionException(e.getMessage(), e);
        }
        return messageList;
    }

    /**
     * Перезаписывает поле mtext, другими словами редактирует сообщение.
     */
    @Override
    public void update(Message message) throws DaoException {
        final String sql = "UPDATE Message SET mtext = ? WHERE mess_id = ?";
        try (Connection conn = dataSource.getConnection();
             Connection actual = ((PooledConnection) conn).getConnection();
             PreparedStatement preparedStatement = actual.prepareStatement(sql)
        ) {
            preparedStatement.setString(1, message.getMtext());
            preparedStatement.setInt(2, message.getMessageId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new QueryExecutionException(e.getMessage(), e);
        }
    }

    /**
     * Не используется
     */
    @Override
    public void delete(Message message) throws DaoException {
        throw new UnsupportedOperationException("Not used.");
    }

    /**
     * Возвращает список всех сообщений.
     */
    @Override
    public List<Message> findAll() throws DaoException {
        final List<Message> messageList = new ArrayList<>();
        final String sql = "SELECT * FROM Message";
        try (Connection conn = dataSource.getConnection();
             Connection actual = ((PooledConnection) conn).getConnection();
             PreparedStatement preparedStatement = actual.prepareStatement(sql);
             ResultSet resultSet = preparedStatement.executeQuery()
        ) {
            while (resultSet.next()) {
                Message message = generateMessage(resultSet);
                messageList.add(message);
            }
        } catch (SQLException e) {
            throw new QueryExecutionException(e.getMessage(), e);
        }
        return messageList;
    }
}
