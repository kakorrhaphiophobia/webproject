package com.webproject.entity;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Объектное представление сущности Chat
 */
public class Chat implements Serializable {

    private int chatId;
    private String userOne;
    private String userTwo;
    private Timestamp dateCreation;

    public Chat() {
    }

    public Chat(String userOne, String userTwo, Timestamp dateCreation) {

        this.userOne = userOne;
        this.userTwo = userTwo;
        this.dateCreation = dateCreation;
    }

    public int getChatId() {
        return chatId;
    }

    public String getUserOne() {
        return userOne;
    }

    public void setUserOne(String userOne) {
        this.userOne = userOne;
    }

    public String getUserTwo() {
        return userTwo;
    }

    public void setUserTwo(String userTwo) {
        this.userTwo = userTwo;
    }

    public Timestamp getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Timestamp dateCreation) {
        this.dateCreation = dateCreation;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        Chat chat = (Chat) obj;

        if (chatId != chat.chatId) return false;
        if (userOne.equals(chat.userOne)) return false;
        if (userTwo.equals(chat.userTwo)) return false;
        return dateCreation.equals(chat.dateCreation);
    }

}
