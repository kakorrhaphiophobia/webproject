package com.webproject.entity;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Объектное представление сущности User.
 */
public class User implements Serializable {

    private String login;
    private String password;
    private String email;
    private Timestamp dateRegistration;
    private String role;
    private boolean disable;

    public User() {
    }

    public User(String login, String password, String email, Timestamp dateRegistration) {
        this.login = login;
        this.password = password;
        this.email = email;
        this.dateRegistration = dateRegistration;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Timestamp getDateRegistration() {
        return dateRegistration;
    }

    public void setDateRegistration(Timestamp dateRegistration) {
        this.dateRegistration = dateRegistration;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isDisable() {
        return disable;
    }

    public void setDisable(boolean disable) {
        this.disable = disable;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null) return false;
        if (!(getClass() == obj.getClass())) return false;
        else {
            User temp = (User) obj;
            if ((temp.getLogin().equals(this.login)) &
                    (temp.getPassword().equals(this.password)) &
                    (temp.getEmail().equals(this.email)) &
                    (temp.getDateRegistration().equals(this.dateRegistration)) &
                    (temp.getRole().equals(this.role))) return true;
            else
                return false;
        }
    }
}
