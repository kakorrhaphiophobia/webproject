package com.webproject.entity;

import java.io.Serializable;
import java.sql.Date;


/**
 * Объектное представление сущности Person.
 */
public class Person implements Serializable {
    private String login;
    private String firstName;
    private String lastName;
    private String gender;
    private Date dob;
    private String country;
    private String address;
    private String telephone;

    public Person(String login, String firstName, String lastName, String gender, Date dob, String country, String address, String telephone) {
        this.login = login;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.dob = dob;
        this.country = country;
        this.address = address;
        this.telephone = telephone;
    }

    public Person() {
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (!login.equals(person.login)) return false;
        if (!firstName.equals(person.firstName)) return false;
        if (!lastName.equals(person.lastName)) return false;
        if (!gender.equals(person.gender)) return false;
        if (!dob.equals(person.dob)) return false;
        if (!country.equals(person.country)) return false;
        if (!address.equals(person.address)) return false;
        return telephone.equals(person.telephone);
    }
}
