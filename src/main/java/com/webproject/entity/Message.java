package com.webproject.entity;


import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Объектное представление сущности Message
 */
public class Message implements Serializable {
    private int messageId;
    private int chatId;
    private String sender;
    private String mtext;
    private Timestamp dateAndTime;

    public Message(){

    }

    public Message(int chatId, String sender, String mtext, Timestamp dateAndTime) {
        this.chatId = chatId;
        this.sender = sender;
        this.mtext = mtext;
        this.dateAndTime = dateAndTime;
    }

    public int getMessageId() {
        return messageId;
    }

    public int getChatId() {
        return chatId;
    }

    public void setChatId(int chatId) {
        this.chatId = chatId;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getMtext() {
        return mtext;
    }

    public void setMtext(String mtext) {
        this.mtext = mtext;
    }

    public Timestamp getDateAndTime() {
        return dateAndTime;
    }

    public void setDateAndTime(Timestamp dateAndTime) {
        this.dateAndTime = dateAndTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Message message = (Message) o;

        if (messageId != message.messageId) return false;
        if (chatId != message.chatId) return false;
        if (!sender.equals(message.sender)) return false;
        if (!mtext.equals(message.mtext)) return false;
        return dateAndTime.equals(message.dateAndTime);
    }

}
