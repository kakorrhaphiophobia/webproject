package com.webproject.entity;

import java.io.Serializable;
import java.sql.Date;

/**
 * Объектное представление сущности Friendship.
 */
public class Friendship implements Serializable {
    private String userOne;
    private String userTwo;
    private int status;
    private Date dateRequest;

    public Friendship() {
    }

    public Friendship(String userOne, String userTwo, int status, Date dateRequest) {
        this.userOne = userOne;
        this.userTwo = userTwo;
        this.status = status;
        this.dateRequest = dateRequest;
    }

    public String getUserOne() {
        return userOne;
    }

    public void setUserOne(String userOne) {
        this.userOne = userOne;
    }

    public String getUserTwo() {
        return userTwo;
    }

    public void setUserTwo(String userTwo) {
        this.userTwo = userTwo;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getDateRequest() {
        return dateRequest;
    }

    public void setDateRequest(Date dateRequest) {
        this.dateRequest = dateRequest;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null) return false;
        if (!(getClass() == obj.getClass())) return false;
        else {
            Friendship temp = (Friendship) obj;
            if ((temp.getUserOne().equals(this.userOne)) &
                    (temp.getUserTwo().equals(this.userTwo)) &
                    (temp.getStatus() == this.status) &
                    (temp.getDateRequest().equals(this.dateRequest))) return true;
            else
                return false;
        }
    }
}
