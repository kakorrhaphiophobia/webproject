package com.webproject.service;

import com.webproject.entity.User;

/**
 * Потокобезопсный контест конкретного пользователя, хранилище аутентификационных данных.
 * */
public class SecurityContexUserHolder {

    private static final ThreadLocal<User> threadLocalScope = new  ThreadLocal<>();

    public final static User getLoggedUser() {
        return threadLocalScope.get();
    }

    public final static void setLoggedUser(User user) {
        threadLocalScope.set(user);
    }

}
