package com.webproject.service;

import com.webproject.dao.UsersDao;
import com.webproject.entity.User;
import com.webproject.exceptions.service.InvalidPasswordException;

import javax.sql.DataSource;

public class JdbcService {

    /**
     * Находит объект Пользователя в БД согласно его логину и паролю.
     * Возвращает объект Пользователя из БД если введенные логин и пароль верны.
     * */
    public User find(DataSource dataSource, String login, String password) throws InvalidPasswordException {
        final User user = new UsersDao(dataSource).findByLogin(login);
        String passwordDB = user.getPassword();
        if (passwordDB.equals(password)) {
            return user;
        } else
            throw new InvalidPasswordException("Invalid password.");//TODO Подумать как избежать этой ситуации
    }
}