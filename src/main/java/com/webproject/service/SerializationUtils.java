package com.webproject.service;

import java.io.*;
import java.util.Map;

public class SerializationUtils {
    public static byte[] serialize(Map<String, Object> stringObjectMap) throws IOException {
       final byte[] serializedMap;
        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
            try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream)) {
                objectOutputStream.writeObject(stringObjectMap);
            }
            serializedMap = byteArrayOutputStream.toByteArray();
        }
        return serializedMap;
    }

    public static Object deserialize(byte[] bytes) throws IOException, ClassNotFoundException {
        try(ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes)){
            try(ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream)){
                return objectInputStream.readObject();
            }
        }
    }
}
