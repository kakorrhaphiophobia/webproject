package com.webproject.servlets;


import com.webproject.entity.User;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class UserServlet extends HttpServlet {
    final static Logger logger = Logger.getLogger(UserServlet.class);

    //TODO Проверить статус disable у Пользователя

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        System.out.println(">> User servlet <<");
        HttpSession session = req.getSession();
        synchronized (session) {
            User user = (User) session.getAttribute("PERSON");
            //TODO Проверить логин и пароль из атрибута на валидность вызвав метод из сервиса
            logger.debug("User login from PERSON = " + user.getLogin());
            req.setAttribute("login", user.getLogin());
            req.setAttribute("password", user.getPassword());
            //TODO Вывести таблицу Users;
            RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/jsp/user.jsp");
            dispatcher.forward(req, resp);
        }
    }
}
