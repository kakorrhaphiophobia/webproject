package com.webproject.servlets;

import com.webproject.entity.User;
import com.webproject.listeners.DBCPContextListener;
import com.webproject.service.JdbcService;
import com.webproject.service.SecurityService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.sql.DataSource;
import java.io.IOException;

public class ReloginServlet extends HttpServlet {
    final private JdbcService jdbcService = new JdbcService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        System.out.println("relog get");
        RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/jsp/relogin.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final String login = request.getParameter("login");
        final String password = request.getParameter("pwd");
        final String passwordHash = SecurityService.md5(password).toString();

        System.out.println("relog post");
//        DataSource dataSource = DBCPContextListener.getInstance(getServletContext()).getDataSource();
//        final User user = jdbcService.find(dataSource, login, passwordHash);
//        HttpSession session = request.getSession(true);
//        session.setAttribute("user", user.getLogin());
//        session.setMaxInactiveInterval(60 * 60 * 24);
//        Cookie userCookie = new Cookie("user", user.getLogin());
//        userCookie.setMaxAge(60 * 60 * 24);
//        response.addCookie(userCookie);
//        response.sendRedirect("/user");
    }
}


