package com.webproject.servlets;

import com.webproject.entity.User;
import com.webproject.exceptions.service.InvalidPasswordException;
import com.webproject.listeners.DBCPContextListener;
import com.webproject.service.JdbcService;
import com.webproject.service.SecurityService;
import com.webproject.service.Utility;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import java.io.IOException;

public class AuthorisationServlet extends HttpServlet {
    final static Logger logger = Logger.getLogger(AuthorisationServlet.class);

    final private JdbcService jdbcService = new JdbcService();
    private DataSource dataSource;

    @Override
    public void init() throws ServletException {
        dataSource = DBCPContextListener.getInstance(getServletContext()).getDataSource();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/jsp/home.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final User user;
        final String login = request.getParameter("login");
        final String password = request.getParameter("pwd");


        //TODO DaoException когда поля ввода отправляют пустыми
        if (Utility.empty(login) && Utility.empty(password)) {
            request.setAttribute("error", "Пустые поля ввода.");
            response.sendRedirect("/home");
            return;
        }

        final String hashUserPwd = SecurityService.md5(password).toString();
        HttpSession session = request.getSession();

        try {
            user = jdbcService.find(dataSource, login, hashUserPwd);
            if (user != null) {
                session.setAttribute("PERSON", user);
//                session.setMaxInactiveInterval(60 * 60 * 24);
//                Cookie userCookie = new Cookie("user", user.getLogin());
//                userCookie.setMaxAge(60 * 60 * 24);
//                response.addCookie(userCookie);
                response.sendRedirect("/user");
            }
        } catch (InvalidPasswordException e) {
            logger.error("Invalid input.", e);
            request.setAttribute("error", "Ошибка ввода.");
            response.sendRedirect("/home");
        }
    }
}



