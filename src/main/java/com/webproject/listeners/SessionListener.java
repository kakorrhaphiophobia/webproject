package com.webproject.listeners;

import org.apache.log4j.Logger;

import javax.servlet.http.HttpSessionActivationListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class SessionListener implements HttpSessionListener, HttpSessionActivationListener {
    final static Logger logger = Logger.getLogger(SessionListener.class);

    public SessionListener() {
        super();
    }


    @Override
    public void sessionCreated(HttpSessionEvent sessionEvent) {
        logger.debug("Session created: " + sessionEvent.getSession().getId());
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent sessionEvent) {
        logger.debug("Session destroyed: " + sessionEvent.getSession().getId());

    }

    @Override
    public void sessionWillPassivate(HttpSessionEvent se) {
        logger.debug("Session passivate: " + se.getSession().getId());

    }

    @Override
    public void sessionDidActivate(HttpSessionEvent se) {
        logger.debug("Session created: " + se.getSession().getId());

    }
}
