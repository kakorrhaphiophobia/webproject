package com.webproject.listeners;

import com.webproject.entity.User;
import com.webproject.service.SecurityContexUserHolder;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
/**
 * Слушатель входящих HTTP запросов, который бы при поступлении запроса
 * присоеденяет данные о пользователе к потоку обработки,
 * а по завершении обработки запроса, очищает эту информацию.
 * */
public class AuthenticationSpreadListener implements ServletRequestListener {

    @Override
    public void requestInitialized(ServletRequestEvent event) {
        HttpServletRequest request = (HttpServletRequest) event.getServletRequest();
        HttpSession session = request.getSession(false);
        if (session == null) {
            return;
        }
        User user = (User) session.getAttribute("PERSON");
        SecurityContexUserHolder.setLoggedUser(user);
    }

    @Override
    public void requestDestroyed(ServletRequestEvent event) {
        SecurityContexUserHolder.setLoggedUser(null);
    }

}
